package com.example.redemo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Hello implements Serializable {
    @Id
    private Integer code;
    private Object data;
    private String message;

    public Hello(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}

