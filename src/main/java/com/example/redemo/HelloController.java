package com.example.redemo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    RedisConnectionFactory factory;

    @GetMapping("/")
    public Hello getHello() {
        return new Hello(HttpStatus.OK.value(), "hello world");
    }

    @GetMapping("/healthz")
    public Hello getHealth() {
        return new Hello(HttpStatus.OK.value(), factory.getConnection().ping());
    }
}
