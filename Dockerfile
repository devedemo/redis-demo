FROM maven:3-eclipse-temurin-11-alpine AS builder
WORKDIR /build
COPY . .
RUN mvn clean package spring-boot:repackage -DskipTests

FROM eclipse-temurin:11-jre-alpine
WORKDIR /opt/app
RUN addgroup --system javauser && adduser -S -s /usr/sbin/nologin -G javauser javauser
COPY --from=builder /build/target/demo-redis-0.0.1-SNAPSHOT.jar app.jar
COPY --from=builder /build/src/main/resources/application.properties /etc/app/application.properties
RUN chown -R javauser:javauser . && chown -R javauser:javauser /etc/app
USER javauser
ENV REDIS_HOST localhost
ENV REDIS_PORT 6379
ENV REQ_TIMEOUT 200
EXPOSE 8080
HEALTHCHECK --interval=30s --timeout=3s --retries=1 CMD wget -qO- http://localhost:8080/healthz/ | grep PONG || exit 1
ENTRYPOINT ["java", "-jar", "-Dspring.config.location=/etc/app/application.properties", "app.jar"]